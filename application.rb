#require "sinatra"
require "watir"
require "watir-webdriver/wait"

DEFAULT_TYPE = "external"

#types = ["external", "game", "post", "mobile", "shop", "group"]

browser = Watir::Browser.start('https://target.my.com/#login')

Watir::Wait.until { browser.ready_state }

browser.text_field(:name, "login").when_present.set("+79534874744")
browser.text_field(:name, "password").when_present.set("qwerty22")
browser.button(:class, "button_submit button").when_present.click

browser.link(:class, "campaign-toolbar__create-button").when_present.click

browser.div(:class, "product-types__item_#{DEFAULT_TYPE}").when_present.click

#browser.close